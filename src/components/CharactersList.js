import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchStarWarCharacters, filterCharacters, toggleDetailView } from '../actions';
import CharactersListItem from './CharactersListItem';

class CharactersList extends Component {

  componentWillMount(){
    this.props.fetchStarWarCharacters();
  }

  applyFilter(query) {
    this.props.filterCharacters(query);
  }

  toggleDetailView(characterId) {
    this.props.toggleDetailView(characterId);
  }

  render() {
    const { charactersList, searchQuery } = this.props;
    let characters = searchQuery ? [] : charactersList;

    if(searchQuery) { // if there is search query, filter the list
      characters = charactersList.filter(aCharacter => {
        return aCharacter.name.toLowerCase().search(searchQuery.toLowerCase()) !== -1;
      })
    }

    return (
      <div className='dv-characters-container'>
        <div className='dv-search-box'>
          <div className='dv-search-wrap'>
            <input placeholder='Search' className='search-charaters-input' onChange={event => this.applyFilter(event.target.value)}/>
            <i className="search-icon fa fa-search"></i>
          </div>
        </div>
        <ul className='characters-list'>
          {characters.map((character, index) => {
              return (
                <li className='list-item' key={index}>
                  <CharactersListItem character={character} toggleDetailView={()=>this.toggleDetailView(character.id)} />
                </li>
              )
            })}
        </ul>
      </div>
    )
  }
}

const mapStateToProps = ({ starwars }) => {
  const { charactersList, searchQuery } = starwars;
  return { charactersList, searchQuery };
}

export default connect(mapStateToProps, { fetchStarWarCharacters, filterCharacters, toggleDetailView })(CharactersList);
