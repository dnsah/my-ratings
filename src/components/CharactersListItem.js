import React, { Component } from 'react';
import Rating from './Rating';

class CharactersListItem extends Component {

  renderReature() {
    const { features } = this.props.character;
      return features.map((feature, index) => {
        return <Rating key={index} feature={feature} characterId={this.props.character.id}/>
        });
  }

  renderDetailView(character) {
    if(!character.detailView)
    return;

    return (
      <div className='dv-deatil-view'>
        <ul>
          <li>
            <div className='dv-details-item-container'>
              <div className='dv-details-item-label-left'>Name</div>
              <div className='dv-details-item-label-right'>{character.name}</div>
              <div className='dv-detail-item-comments'>Comments</div>
            </div>
          </li>
          <li>
            <div className='dv-details-item-container'>
              <div className='dv-details-item-label-left'>Height</div>
              <div className='dv-details-item-label-right'>{character.height}</div>
              <div className='dv-detail-item-comments'><span className='comment-pencil-icon fa fa-pencil'/><input className='detail-item-comments-input' placeholder='comments'/></div>
            </div>
          </li>
          <li>
            <div className='dv-details-item-container'>
              <div className='dv-details-item-label-left'>Mass</div>
              <div className='dv-details-item-label-right'>{character.mass}</div>
              <div className='dv-detail-item-comments'><span className='comment-pencil-icon fa fa-pencil'/><input className='detail-item-comments-input' placeholder='comments'/></div>
            </div>
          </li>
          <li>
            <div className='dv-details-item-container'>
              <div className='dv-details-item-label-left'>Hair Color</div>
              <div className='dv-details-item-label-right'>{character.hair_color}</div>
              <div className='dv-detail-item-comments'><span className='comment-pencil-icon fa fa-pencil'/><input className='detail-item-comments-input' placeholder='comments'/></div>
            </div>
          </li>
          <li>
            <div className='dv-details-item-container'>
              <div className='dv-details-item-label-left'>Skin Color</div>
              <div className='dv-details-item-label-right'>{character.skin_color}</div>
              <div className='dv-detail-item-comments'><span className='comment-pencil-icon fa fa-pencil'/><input className='detail-item-comments-input' placeholder='comments'/></div>
            </div>
          </li>
          <li>
            <div className='dv-details-item-container'>
              <div className='dv-details-item-label-left'>Eye Color</div>
              <div className='dv-details-item-label-right'>{character.eye_color}</div>
              <div className='dv-detail-item-comments'><span className='comment-pencil-icon fa fa-pencil'/><input className='detail-item-comments-input' placeholder='comments'/></div>
            </div>
          </li>
          <li>
            <div className='dv-details-item-container'>
              <div className='dv-details-item-label-left'>Birth Year</div>
              <div className='dv-details-item-label-right'>{character.birth_year}</div>
              <div className='dv-detail-item-comments'><span className='comment-pencil-icon fa fa-pencil'/><input className='detail-item-comments-input' placeholder='comments'/></div>
            </div>
          </li>
          <li>
            <div className='dv-details-item-container'>
              <div className='dv-details-item-label-left'>Gender</div>
              <div className='dv-details-item-label-right'>{character.gender}</div>
              <div className='dv-detail-item-comments'><span className='comment-pencil-icon fa fa-pencil'/><input className='detail-item-comments-input' placeholder='comments'/></div>
            </div>
          </li>
          <li>
            <div className='dv-details-item-container'>
              <div className='dv-details-item-label-left'>Homeworld</div>
              <div className='dv-details-item-label-right'>{character.planetOrigin}</div>
              <div className='dv-detail-item-comments'><span className='comment-pencil-icon fa fa-pencil'/><input className='detail-item-comments-input' placeholder='comments'/></div>
            </div>
          </li>
        </ul>
      </div>
    )
  }

  renderOverallRatings(character) {
    const { overallRatings, nbrStars } = character.ratings;
    const starCount = Math.floor(nbrStars);
    let stars;
    switch (starCount) {
      case 1:
        stars = <span>&#9733; &#9734; &#9734; &#9734; &#9734;</span>
        break
      case 2:
        stars = <span>&#9733; &#9733; &#9734; &#9734; &#9734;</span>
        break;
      case 3:
        stars = <span>&#9733; &#9733; &#9733; &#9734; &#9734;</span>
        break;
      case 4:
        stars = <span>&#9733; &#9733; &#9733; &#9733; &#9734;</span>
        break;
      case 5:
        stars = <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span>
        break;
      default:
        stars = <span>&#9734; &#9734; &#9734; &#9734; &#9734;</span>
        break;
    }
    return (
      <div className='dv-overall-rating'>
        <div className='dv-lbl-overall'>
          <div className='dv-overall-lbl-container'>
            <div className='div-overall-lbl-left'>Overall Votes : </div>
            <div className='div-overall-lbl-right'>{overallRatings}</div>
          </div>
          <div className='dv-overall-lbl-container'>
            <div className='div-overall-lbl-left'>Overall Ratings ({nbrStars}) : </div>
            <div className='div-overall-lbl-right'><span className='stars-icon'>{stars}</span></div>
          </div>
        </div>
        {this.renderDetailView(character)}
      </div>
    )
  }

  render() {
    const { character } = this.props;
    return (
      <div className='dv-characters-list-item-content'>
        <div className='dv-list-item-wrapper'>
        <div onClick={()=>this.props.toggleDetailView()}>
          <div className='dv-character-name'>{character.name}</div>
          <div className='dv-character-planet'>Planet of origin : {character.planetOrigin}</div>
        </div>
          {this.renderReature()}
        </div>
        {this.renderOverallRatings(character)}
      </div>
    )
  }
}

export default CharactersListItem;
