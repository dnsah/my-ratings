import React, { Component } from 'react';
import { connect } from 'react-redux';
import { voteCharacter } from '../actions';

class Rating extends Component {

  voteCharacter(vote) {
    const {characterId, feature } = this.props;
    this.props.voteCharacter({characterId, feature, vote});
  }

  render() {
    const { title, value, votes } = this.props.feature;
    return (
        <div className='rating-container'>
          <div className='character-features'>
            <div className='feature-title'>{title}</div>
            <div className='feature-value'>{value}</div>
          </div>
          <div className='rating-box'>
            <div className='vote-counter'>{votes}</div>
            <div className='vote-down'>
              <button className='vote-btn' onClick={() => this.voteCharacter('VOTE_DOWN')}><span className='vote-icon fa fa-thumbs-down'></span></button>
            </div>
            <div className='vote-up'>
              <button className='vote-btn' onClick={() => this.voteCharacter('VOTE_UP')}><span className='vote-icon fa fa-thumbs-up'></span></button>
            </div>
          </div>
        </div>
    )
  }
}

const mapStateToProps = ({ starwars }) => {
  const { charactersList } = starwars;
  return { charactersList };
}

export default connect(mapStateToProps, { voteCharacter })(Rating);
