import React, { Component } from 'react';
import CharactersList from './components/CharactersList';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className='App'>
        <div className='dv-header-title'>
          <span> Rate Star Wars Characters</span>
        </div>
        <CharactersList />
        <footer className='dv-footer'>
        <div className='dv-footer-separator' />
          <span>Developer &copy; <a href='http://my-ratings.netlify.com' target='_blank' rel="noopener noreferrer">dhrubsah</a></span>
        </footer>
      </div>
    )
  }
}

export default App;
