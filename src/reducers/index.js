import { combineReducers } from 'redux';
import CharactersReducer from './CharactersReducer';

export default combineReducers({
  starwars: CharactersReducer
});
