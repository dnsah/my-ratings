import * as mathUtils from '../utils/mathUtils';
import {
  FETCH_CHARACTERS_SUCCESS,
  PLANET_OF_ORIGIN_LOADED,
  FILTER_CHARACTERS,
  VOTE_CHARACTER,
  TOGGLE_DETAIL_VIEW
} from '../actions/types';

const INITIAL_STATE = {
  charactersList: [],
  searchQuery: ''
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_CHARACTERS_SUCCESS:
      return {...state, charactersList: sortCharacters(action.payload)}

    case FILTER_CHARACTERS:
      return {...state, charactersList: sortCharacters(state.charactersList),  searchQuery: action.payload}

    case VOTE_CHARACTER:
      return { ...state,
          charactersList: state.charactersList.map(character => character.id === action.payload.characterId ?
          { ...character, feature: action.payload.feature, ratings: mathUtils.getOverallRatings(character) } : character)
      };

    case PLANET_OF_ORIGIN_LOADED:
      return { ...state,
          charactersList: state.charactersList.map(character => character.id === action.payload.characterId ?
          { ...character, planetOrigin: action.payload.planetOrigin, features: character.features.map(feature=> feature.key ==='planetOrigin' ? {...feature, planetOrigin: action.payload.planetOrigin}: feature) } : character)
      };

    case TOGGLE_DETAIL_VIEW:
      return { ...state,
          charactersList: state.charactersList.map(character => character.id === action.payload ?
          { ...character, detailView: !character.detailView } : {...character, detailView: false })
      };

    default:
      return {...state };
  }
}

const sortCharacters = (charactersList) => {
  return charactersList.sort((a, b) => {
    return a.ratings.overallRatings < b.ratings.overallRatings;
  })
}
