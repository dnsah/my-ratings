import {
  FETCH_CHARACTERS_SUCCESS,
  PLANET_OF_ORIGIN_LOADED
} from './types';


export const fetchStarWarCharacters = () => {
  //fetch from api
  return (dispatch) => {
    const BASE_URL = 'https://swapi.co/api/people/?page=1';
    fetch(BASE_URL,{
      method: 'GET'
    })
    .then(response => response.json())
    .then(json => {
      const data = preProcess(json.results);
      dispatch({
        type: FETCH_CHARACTERS_SUCCESS,
        payload: data
      })

      //load planet of orinin
      data.forEach(dt => {
        loadPlanetOfOrigin(dt, dispatch);
      })

    })
  }
}

const preProcess = (data, dispatch) => {
  if(!data || !data.length)
    return;
    data.forEach((dt, index) => {
      //for the sake of this exercise, lets only have 3 features for ratings
      const { hair_color, skin_color, eye_color } = dt;
      dt.id = index;
      dt.detailView = false;
      dt.ratings = {overallRatings:0, nbrStars:0};
      dt.planetOrigin = '';
      dt.features = [];
      dt.features.push({key: 'hair_color', title: 'Hair Color', value: hair_color, votes: 0});
      dt.features.push({key: 'skin_color', title: 'Skin Color', value: skin_color, votes: 0});
      dt.features.push({key: 'eye_color', title: 'Eye Color', value: eye_color, votes: 0});
    });

  return data;
}

const loadPlanetOfOrigin = (character , dispatch) => {
  const url = character.homeworld;
  const characterId = character.id;
  fetch(url,{
    method: 'GET'
  })
  .then(response => response.json())
  .then(json => {
    const planetOrigin = json.name;
    dispatch({
      type: PLANET_OF_ORIGIN_LOADED,
      payload: {characterId, planetOrigin}
    })
  })
}
