import {
  FILTER_CHARACTERS,
  VOTE_CHARACTER,
  TOGGLE_DETAIL_VIEW
} from './types';

export const filterCharacters = (query) => {
  return {
    type: FILTER_CHARACTERS,
    payload: query
  }
}

export const voteCharacter = ({characterId, feature, vote }) => {
  //limit the votes to be >= 0 and <= 100
  if(vote === 'VOTE_UP' && feature.votes < 100)
    feature.votes++;
  else if (vote === 'VOTE_DOWN' && feature.votes > 0)
   feature.votes--;

  return {
    type: VOTE_CHARACTER,
    payload: { characterId, feature }
  }
}

export const toggleDetailView = (characterId) => {
    return {
      type: TOGGLE_DETAIL_VIEW,
      payload: characterId
    }
}
