
export const getOverallRatings = (character) => {
  const { features } = character;
  const total_categories = features.length;

  //overall rating is calculated by taking 100 - square root of
  //( sum sqaure of individual votes divided by number of categories )
  //sum square of individual scores
  let sum = 0;

  features.forEach(aFeature => {
    sum = sum + ( (100-aFeature.votes) * (100-aFeature.votes) );
  });

  const overallRatings = (100 - Math.sqrt(sum/total_categories)).toFixed(2)/1;
  const nbrStars = (overallRatings / 20).toFixed(1)/1;
  return { overallRatings, nbrStars };
}
